﻿using System;
using Marsen.WebKata.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using TechTalk.SpecFlow;

namespace Marsen.WebKata.Tests
{
    [Binding]
    public class GameInitialSteps
    {
        private IWebDriver driver;
        private string anwser;

        [BeforeScenario]
        public void SetupTest()
        {
            driver = new FirefoxDriver();
        }

        [Given(@"Answer is Chaifeng")]
        public void GivenAnswerIsChaifeng()
        {
            var hangmanService = new HangmanService();
            anwser = hangmanService.GetAnswer();
        }
        
        [When(@"Game Start")]
        public void WhenGameStart()
        {
            driver.Navigate().GoToUrl("http://kata.marsen.dev/");
        }
        
        [Then(@"Length is ""(.*)""")]
        public void ThenLengthIs(int p0)
        {
            var expect = anwser.Length.ToString();
            Assert.IsTrue(driver.FindElement(By.Id("length")).Text.Contains(expect));
        }
        
        [Then(@"Times is ""(.*)""")]
        public void ThenTimesIs(int p0)
        {
            Assert.IsTrue(driver.FindElement(By.Id("times")).Text.Contains("12"));
        }
        
        [Then(@"Used is ""(.*)""")]
        public void ThenUsedIs(string p0)
        {
            Assert.IsTrue(driver.FindElement(By.Id("used")).Text.Contains("AEIOU"));
        }
        
        [Then(@"Discovered is ""(.*)""")]
        public void ThenDiscoveredIs(string p0)
        {
            
            Assert.IsTrue(driver.FindElement(By.Id("discovered")).Text.Contains("__AI_E__"));
        }
    }
}
