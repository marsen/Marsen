﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Marsen.Core.Notify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marsen.Core.Request;
using NSubstitute;

namespace Marsen.Core.Notify.Tests
{
    [TestClass()]
    public class NotifyTest
    {
        [TestMethod()]
        [TestCategory("Unit Test")]
        public void 呼叫SlackNotify_Send後應該執行Request_Load()
        {
            ////arrange
            var req = Substitute.For<IRequest>();
            SlackNotify target = (SlackNotify)NotifyFactory.GetNotify(Notify.Slack);
            target.SetRequest(req);
            //// 實際使用應呼叫Setting方法來設定token,channel,username參數
            //// doc : https://api.slack.com/
            //// token : https://api.slack.com/docs/oauth-test-tokens
            //// 範例如下:
            //// target.Setting("xoxp-***********-***********-***********-***********", "general", "iamrobot");
            ////act
            target.Send("test slack notify");
            ////assert
            req.Received().Load(Arg.Any<string>());
        }

        [TestMethod()]
        [TestCategory("Unit Test")]
        public void 呼叫GitterNotify_Send後應該執行Request_Load()
        {
            ////arrange
            var req = Substitute.For<IRequest>();
            GitterNotify target = (GitterNotify)NotifyFactory.GetNotify(Notify.Gitter);
            //// 實際使用應呼叫Setting方法來設定token,roomId參數
            //// doc : https://developer.gitter.im/docs/welcome
            //// token : https://developer.gitter.im/apps
            //// target.Setting("****************************************", "************************");
            target.SetRequest(req);
            ////act

            target.Send("test gitter notify");
            ////assert
            req.Received().Load(Arg.Any<string>());
        }

    }
}