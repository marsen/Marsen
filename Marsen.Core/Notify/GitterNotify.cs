﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marsen.Core.Request;

namespace Marsen.Core.Notify
{
    public class GitterNotify : INotify
    {
        private IRequest request;
        /// <summary>
        /// https://api.gitter.im/v1/rooms/:roomId/chatMessages
        /// </summary>
        private const string API = "https://api.gitter.im/v1/rooms";

        
        /// <summary>
        /// postData
        /// </summary>
        private Dictionary<string, string> postData = new Dictionary<string, string>();

        public static INotify CreateNotify()
        {
            var postData = new Dictionary<string, string>();
            var request = (BaseRequest)RequsetFactory.GetRequest(string.Empty, postData);
            request.AddHeader("Accept", "application/json");
            
            //TODO:SET HEAD
            return new GitterNotify(request);
        }


        public void Setting(string token, string roomId)
        {
            var uri = string.Format("{0}/{1}/chatMessages", API, roomId);
            this.request.SetUri(uri);
            var authorization = string.Format("Bearer {0}", token);
            this.request.AddHeader("AUTHORIZATION", authorization);
        }
        private GitterNotify(IRequest requeset)
        {
            this.request = requeset;
        }

        public void Send(string text)
        {
            this.request.AddPostdata("text", text);
            this.request.Load("application/json");
        }

        internal void SetRequest(IRequest req)
        {
            this.request = req;
        }
    }
}
