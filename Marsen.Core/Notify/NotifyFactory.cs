﻿using System;

namespace Marsen.Core.Notify
{
    public static class NotifyFactory
    {
        public static INotify GetNotify(string token, string channel, string username)
        {
            //// if some logical
            return SlackNotify.CreateNotify(token, channel, username);
        }

        public static INotify GetNotify(Notify notify)
        {
            switch (notify)
            {
                case Notify.Slack:
                    return SlackNotify.CreateNotify();

                case Notify.Gitter:
                    return GitterNotify.CreateNotify();

                default:
                    throw new Exception("unkonw notify type");
            }
        }
    }
}