﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marsen.Core.Notify
{
    public enum Notify
    {
        /// <summary>
        /// Slack
        /// </summary>
        Slack,

        /// <summary>
        /// Gitter
        /// </summary>
        Gitter
    }
}
