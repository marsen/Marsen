﻿using System.Collections.Generic;
using Marsen.Core.Request;

namespace Marsen.Core.Notify
{
    /// <summary>
    /// SlackNotify
    /// </summary>
    public class SlackNotify : INotify
    {
        /// <summary>
        /// slack api
        /// </summary>
        private const string API = "https://slack.com/api/chat.postMessage";

        /// <summary>
        /// Request
        /// </summary>
        private IRequest request;

        internal static INotify CreateNotify()
        {
            var postData = new Dictionary<string, string>();
            var request = RequsetFactory.GetRequest(API, postData);
            return new SlackNotify(request);
        }

        internal void SetRequest(IRequest req)
        {
            this.request = req;
        } 

        /// <summary>
        /// Initializes a new instance of the SlackNotify class.
        /// </summary>
        /// <param name="request">request</param>
        private SlackNotify(IRequest request)
        {
            this.request = request;
        }

        /// <summary>
        /// CreateNotify
        /// </summary>
        /// <param name="token">slack token</param>
        /// <param name="channel">channel</param>
        /// <param name="username">username</param>
        /// <returns>INotify Object</returns>
        public static INotify CreateNotify(string token, string channel, string username)
        {
            var postData = new Dictionary<string, string>();
            postData.Add("token", token);
            postData.Add("channel", "#" + channel);
            postData.Add("username", username);
            var request = RequsetFactory.GetRequest(API, postData);
            return new SlackNotify(request);
        }

        /// <summary>
        /// Send Messages to Notify
        /// </summary>
        /// <param name="text">text</param>
        public void Send(string text)
        {
            this.request.AddPostdata("text", text);
            this.request.Load();
        }

        /// <summary>
        /// 設定slack參數
        /// </summary>
        /// <param name="token">token</param>
        /// <param name="channel">channel</param>
        /// <param name="username">username</param>
        public void Setting(string token, string channel, string username)
        {
            this.request.AddPostdata("token", token);
            this.request.AddPostdata("channel", channel);
            this.request.AddPostdata("username", username);
        }
    }
}