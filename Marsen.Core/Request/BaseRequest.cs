﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Marsen.Core.Request
{
    /// <summary>
    /// BaseRequest
    /// </summary>
    public class BaseRequest : IRequest
    {
        private class BaseRequestHeader
        {
            public string Accept;
            public string Authorization;
        }

        /// <summary>
        /// requestUri
        /// </summary>
        private string requestUri;

        /// <summary>
        /// requesetMethod
        /// </summary>
        private string requesetMethod = "POST";

        /// <summary>
        /// requesetData
        /// </summary>
        private Dictionary<string, string> requesetData;

        /// <summary>
        /// HttpClient
        /// </summary>
        private HttpClient client;

        /// <summary>
        /// requesetTimeout
        /// </summary>
        private int requesetTimeout = 30;

        private BaseRequestHeader requestHeader;


        /// <summary>
        /// Initializes a new instance of the BaseRequest class.
        /// </summary>
        public BaseRequest()
        {
        }

        /// <summary>
        /// Initializes a new instance of the BaseRequest class.
        /// </summary>
        /// <param name="uri">uri</param>
        /// <param name="data">data</param>
        /// <param name="method">method</param>
        /// <param name="timeout">timeout</param>
        private BaseRequest(string uri, Dictionary<string, string> data, string method = "POST", int timeout = 30)
        {
            this.client = new HttpClient();
            this.requestHeader = new BaseRequestHeader();
            this.requestUri = uri;
            this.requesetData = data;
            this.requesetMethod = method;
            this.requesetTimeout = timeout;
        }

        /// <summary>
        /// CreateRequest
        /// </summary>
        /// <param name="uri">uri</param>
        /// <param name="data">data</param>
        /// <param name="method">method</param>
        /// <param name="timeout">timeout</param>
        /// <returns>IRequest</returns>
        public static IRequest CreateRequest(string uri, Dictionary<string, string> data, string method = "POST", int timeout = 30)
        {
            return new BaseRequest(uri, data, method, timeout);
        }

        /// <summary>
        /// 載入,發動Requeset,取得Response
        /// </summary>
        /// <param name="contentType">contentType</param>
        /// <returns>string</returns>
        public string Load(string contentType = null)
        {


            if (this.requesetMethod == "GET")
            {
                return this.GetAsync().Result;
            }
            else
            {
                return this.PostAsync(contentType).Result;
            }

        }
        /// <summary>
        /// AddPostdata
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        public void AddPostdata(string key, string value)
        {
            this.requesetData.Add(key, value);
        }

        /// <summary>
        /// AddHeader
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        public void AddHeader(string key, string value)
        {
            if (key.ToUpper() == "ACCEPT")
            {
                this.requestHeader.Accept = value;
            }
            if (key.ToUpper() == "AUTHORIZATION")
            {
                this.requestHeader.Authorization = value;
            }
        }

        /// <summary>
        /// SetUri
        /// </summary>
        /// <param name="uri">uri</param>
        public void SetUri(string uri)
        {
            this.requestUri = uri;
        }

        /// <summary>
        /// Async Request by Method  Post
        /// </summary>
        /// <returns>Task<string></returns>
        internal virtual async Task<string> PostAsync(string contentType = null)
        {
            using (client)
            {
                var form = new FormUrlEncodedContent(this.requesetData);
                SetHeader();
                if (string.IsNullOrEmpty(contentType) == false)
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                }
                
                var response = await client.PostAsync(this.requestUri, form);
                ////will throw an exception if not successful
                response.EnsureSuccessStatusCode();
                string content = await response.Content.ReadAsStringAsync();
                return content;
            }
        }
        
        /// <summary>
        /// Async Request by Method Get
        /// </summary>
        /// <returns>Task<string></returns>
        internal virtual async Task<string> GetAsync(string contentType = null)
        {
            using (var client = new HttpClient())
            {

                HttpContent body = new StringContent(null);
                if (string.IsNullOrEmpty(contentType) == false)
                {
                    body.Headers.ContentType = new MediaTypeHeaderValue(contentType);
                }
                
                var q = string.Empty;
                foreach (var data in this.requesetData)
                {
                    q += data.Key + "=" + data.Value;
                }

                var response = await client.GetAsync(this.requestUri + q);

                ////will throw an exception if not successful
                response.EnsureSuccessStatusCode();

                string content = await response.Content.ReadAsStringAsync();
                return content;
            }
        }

        /// <summary>
        /// SetHeader
        /// </summary>
        private void SetHeader()
        {
            if (string.IsNullOrEmpty(this.requestHeader.Accept) == false)
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(this.requestHeader.Accept));
            }
            if (string.IsNullOrEmpty(this.requestHeader.Authorization) == false)
            {
                var authorization = requestHeader.Authorization.Split(' ');
                //// http authorization 應該為「type token」的型式
                //// 如「basic aHR0cHdhdGNoOmY=」
                if (authorization.Length >= 2)
                {
                    var type = authorization[0];
                    var token = authorization[1];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(type,token);
                }
            }
        }


    }
}