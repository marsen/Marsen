﻿namespace Marsen.Core.Request
{
    /// <summary>
    /// IRequest
    /// </summary>
    public interface IRequest
    {
        /// <summary>
        /// Load
        /// </summary>
        /// <returns>string</returns>
        string Load(string contentType = null);

        void AddPostdata(string key, string value);

        /// <summary>
        /// AddHeader
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        void AddHeader(string key, string value);

        /// <summary>
        /// SetUri
        /// </summary>
        /// <param name="uri">uri</param>
        void SetUri(string uri);
    }
}