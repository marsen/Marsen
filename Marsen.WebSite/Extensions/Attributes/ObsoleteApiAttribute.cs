﻿using System;
using System.Web.Mvc;

namespace NineYi.WebStore.WebAPI.Extensions.Attributes
{
    /// <summary>
    /// ObsoleteApiAttribute提供.NET MVC的標示WEBAPI已過期，
    /// 當呼叫到已標示過期的WEBAPI，將回傳一Json並提示過期訊息。
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class ObsoleteApiAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// message
        /// </summary>
        private string message;

        /// <summary>
        /// Initializes a new instance of the <see cref="ObsoleteApiAttribute" /> class.
        /// </summary>
        public ObsoleteApiAttribute()
        {
            this.message = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObsoleteApiAttribute" /> class.
        /// </summary>
        /// <param name="message">message</param>
        public ObsoleteApiAttribute(string message)
        {
            this.message = message;
        }

        /// <summary>
        /// OnActionExecuted
        /// </summary>
        /// <param name="filterContext">filterContext</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            var obsoleteApi = new ObsoleteApiController(this.message);
            filterContext.Result = obsoleteApi.GetObsoleteResult();
        }

        /// <summary>
        /// ObsoleteApiController
        /// </summary>
        private class ObsoleteApiController : Controller
        {
            /// <summary>
            /// message
            /// </summary>
            private string message;

            /// <summary>
            /// Initializes a new instance of the <see cref="ObsoleteApiController" /> class.
            /// </summary>
            public ObsoleteApiController()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ObsoleteApiController" /> class.
            /// </summary>
            /// <param name="message">message</param>
            public ObsoleteApiController(string message)
            {
                if (string.IsNullOrEmpty(message))
                {
                    this.message = "API已過時";
                }
                else
                {
                    this.message = message;
                }
            }

            /// <summary>
            /// GetObsoleteResult
            /// </summary>
            /// <returns>ActionResult</returns>
            public ActionResult GetObsoleteResult()
            {
                return this.Json(new { msg = this.message });
            }
        }
    }
}