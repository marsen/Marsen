# 筆記

# Demo Case

## Case 1. ClickJacking
1. ### 簡介
  ClickJacking 是一種網頁攻擊手法，用以綁架使用者的點擊動作。  
  [Wiki](https://en.wikipedia.org/wiki/Clickjacking)      
2. ### 範例
  [Demo](http://marsen.dev/demo/clickjacking)  
  攻擊者，可以透過隱藏iframe的方式，偽造成受害的網站，並以劫持使用者的點擊行為。  

  `<iframe src="http://clickjackingable/" style="width:100%; height:6755px;">
</iframe>`  
  一般的網站，若未加防範，將會成為攻擊者的誘餌，   
  例如: www.mobile01.tw (2016/06/14)
3. ### 防範
  設定HTTP header 的 `X-Frame-Options`  
  `DENY`  不允許任何外部網站使用iframe嵌入  
  `SAMEORIGIN`  只允許同網站使用iframe嵌入  
  `ALLOW-FROM http://somesite.url/` 允許來自指定來源的網站使用iframe嵌入。

## Case 2. Content Sniffing
1. ### 簡介
  Content Sniffing 也稱作Mime Sniffing 
  是一種瀏覽器的行為，當瀏覽器載入未知的檔案，會猜測其檔案類型。  
  [Wiki](https://en.wikipedia.org/wiki/Content_sniffing)  
2. ### 風險
  當使用者預期載入的檔案或圖片，被猜測為執行檔或指令碼，將有機會引發攻擊。
3. ### 範例
  [Demo](http://marsen.dev/demo/MimeSniffing.zip)  
  註:實測使用IE9(os為WIN10)，測試前請移除web.config這段設定
        <system.webServer>
          <httpProtocol>
            <customHeaders>
              <add name="X-Content-Type-Options" value="nosniff" />
            </customHeaders>
          </httpProtocol>      
        </system.webServer>

4. ### 防範
  設定HTTP header 的 `X-Content-Type-Options`  為 `nosniff` ，  
  明確的禁止瀏覽器作檔案類別的猜測。  
  在.NET 中只需要設定web.config，如步驟3所提。
