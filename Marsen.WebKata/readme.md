# Marsen.Kata

## 目標: 練習

### 練習一 GOOS 學習筆記

- 目的 (業務需求)

    實作[猜字遊戲](http://www.twoplayergames.org/play/668-hangman.html)
- 分析
    - 畫面  
    **times** 為 12 ,表示玩家可以猜測的次數  
    **length** 為一個數字,表示答案單字的字串長度  
    **used** 預設為AEIOU,表示猜測過的字母  
    **discovered** 顯示猜中的字母,尚未猜中以「 _ 」 符號表示  
    - 說明  
    預設會有一個單字作為**答案**,  
    AEIOU所有母音會在**discovered**顯示出來  
    子音會以「 _ 」 符號表示。  
    每次玩家選擇一個字母  
    若猜中答案,**discovered** 會顯示其字母,  
    若未猜中,**times** 減1,畫面小人會前進1格  
    當**times**歸0,仍未猜中所有字母,遊戲失敗結束  
    若猜中,遊戲成功結束。   

- 開發  
  - BDD  
  假設**第一個**場景如下:  
  **情境:** *畫面初始化*
  **假設** *答案為Chaifeng*  
  **當** *遊戲開始*  
  **那麼**   
      *Length* 為 **8**     
      *Times* 為 **12**  
      *Used* 為 **AEIOU**   
      *Discovered* 為 ** \_\_AI\_E\_\_**     
  英文版  
  **Scenario:** *Game Initial*  
  **Given:** *Answer is Chaifeng*  
  **When:** *Game Start*  
  **Then:**   
      *Length* is **8**     
      *Times* is **12**  
      *Used* is **AEIOU**   
      *Discovered* is ** \_\_AI\_E\_\_**    
- 實務操作
  - 目的 : 用.NET MVC 重現 RoR 的測試趨動開發流程
  - 步驟 :
    - 建立.NET MVC 專案與測試(使用.Net 4.6.1)
    - 準備好IIS環境，讓網站正常運作。
    - .NET MVC專案會產生一些Code(含html),沒有必要可以移除
    - 安裝Specflow套件
    - 安裝Selenium套件
  - 先寫BDD
      - 寫第一個場境，得到第一個紅燈

- NEXT
