﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Marsen.WebKata.Startup))]
namespace Marsen.WebKata
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
