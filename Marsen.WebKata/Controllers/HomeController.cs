﻿using System.Web.Mvc;
using Marsen.WebKata.Services;

namespace Marsen.WebKata.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var hangmanService = new HangmanService();
            string anwser = hangmanService.GetAnswer();
            ViewBag.Length = anwser.Length;
            ViewBag.Times = 12;
            ViewBag.Used = "AEIOU";
            ViewBag.Discovered = "__AI_E__";
            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}