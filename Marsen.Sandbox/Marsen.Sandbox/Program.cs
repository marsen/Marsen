﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Marsen.Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("start Sandbox");
            Console.WriteLine("=============");
            PlayInSandbox();
            Console.WriteLine("=============");
            Console.WriteLine("end Sandbox");
            Console.ReadLine();
        }


        private static void PlayInSandbox()
        {
            Console.WriteLine(showTheBool());
        }

        private static object showTheBool()
        {
            var istrue = true;
            return bool.TryParse("XXX",out istrue);
        }
    }
}
